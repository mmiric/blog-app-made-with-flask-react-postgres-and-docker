# Example blog app made with Flask React and Docker

environment variables:

Linux or Mac

> export REACT_APP_SERVER_URL=http://localhost:5001/api

Windows

> \$env:REACT_APP_SERVER_URL = "http://localhost:5001/api"

Build images and run the containers:

> docker-compose up --build

Create the database:

> docker-compose exec server python manage.py recreate_db

Seed the database:

> docker-compose exec server python manage.py seed_db

Wanna see the app?

Go to "http://localhost:3001"

To add remove delete and update articles:

Go to "http://localhost:3001/admin" and log in as "test1@gmail.com" with password "secret".
