const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

const convertDateFormat = articles => {
  return articles.reverse().map(article => {
    let updatedDate = new Date(article.created_date);
    if (article.updated_date !== null) {
      updatedDate = new Date(article.updated_date);
    }
    let year = updatedDate.getFullYear();
    let month = updatedDate.getMonth();
    let day = updatedDate.getDate();

    switch (day) {
      case 1 || 21 || 31:
        day = day + 'st';
        break;
      case 2 || 22:
        day = day + 'nd';
        break;
      case 3:
        day = day + 'rd';
        break;
      default:
        day = day + 'th';
    }

    const monthName = months[month];

    article.updated_date = monthName + ' ' + day + ', ' + year;

    return article;
  });
};

export default convertDateFormat;
