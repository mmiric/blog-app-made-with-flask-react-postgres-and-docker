import React, {useState, useEffect} from 'react';
import {Route, Switch, useHistory} from 'react-router-dom';
import Modal from 'react-modal';

import ArticlesList from './components/ArticlesList';
import ArticleDetail from './components/ArticleDetail';
import NavBar from './components/NavBar';
import About from './components/About';
import Footer from './components/Footer';
import LoginForm from './components/LoginForm';
import Message from './components/Message';
import AddArticle from './components/AddArticle';
import UpdateArticle from './components/UpdateArticle';
import NotFound from './components/NotFound';
import convertDateFormat from './Utils';

import './App.css';

const modalStyles = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.75)'
  },
  content: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    border: 0,
    background: '#fff'
  }
};

Modal.setAppElement(document.getElementById('root'));

const App = () => {
  const history = useHistory();

  const [articles, setArticles] = useState([]);
  const [articleId, setArticleId] = useState(null);
  const [article, setArticle] = useState({
    title: '',
    description: '',
    body: ''
  });
  const [accessToken, setAccessToken] = useState(null);
  const [refreshToken, setRefreshToken] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [messageType, setMessageType] = useState(null);
  const [messageText, setMessageText] = useState(null);

  useEffect(() => {
    return history.listen(() => {
      if (history.action === 'POP') {
        handleCloseModal();
      }
    });
  }, [history]);

  useEffect(() => {
    const abortController = new AbortController();
    async function getArticles() {
      try {
        const resp = await fetch(
          `${process.env.REACT_APP_SERVER_URL}/articles`,
          {signal: abortController.signal}
        );
        const json = await resp.json();
        const articles = convertDateFormat(json.data);
        setArticles(articles);
      } catch (err) {
        console.log(err);
      }
    }
    getArticles();
    return () => {
      abortController.abort();
    };
  }, []);

  useEffect(() => {
    const abortController = new AbortController();
    async function validRefresh() {
      const token = window.localStorage.getItem('refreshToken');
      if (token) {
        try {
          const resp = await fetch(
            `${process.env.REACT_APP_SERVER_URL}/auth/refresh`,
            {
              method: 'post',
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({refresh_token: token})
            },
            {signal: abortController.signal}
          );
          if (resp.status === 200) {
            const json = await resp.json();
            setAccessToken(json.access_token);
            setRefreshToken(true);
            window.localStorage.setItem('refreshToken', json.refresh_token);
          } else {
            throw new Error(resp.statusText);
          }
        } catch (err) {
          console.log(err);
        }
      }
    }
    validRefresh();
    return () => abortController.abort();
  }, [accessToken]);

  const getArticles = async () => {
    try {
      const resp = await fetch(`${process.env.REACT_APP_SERVER_URL}/articles`);
      const json = await resp.json();
      const articles = convertDateFormat(json.data);
      setArticles(articles);
    } catch (err) {
      console.log(err);
    }
  };

  const addArticle = async data => {
    try {
      await fetch(`${process.env.REACT_APP_SERVER_URL}/articles`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + accessToken
        },
        body: JSON.stringify(data)
      });
      await getArticles();
      handleCloseModal();
      createMessage('success', 'Article added.');
    } catch (err) {
      console.log(err);
      handleCloseModal();
      createMessage('danger', 'Article already exist.');
    }
  };

  const getArticle = async article_id => {
    try {
      const resp = await fetch(
        `${process.env.REACT_APP_SERVER_URL}/articles/${article_id}`
      );
      const json = await resp.json();
      setArticle(json.data);
      setArticleId(article_id);
    } catch (err) {
      console.log(err);
    }
  };

  const updateArticle = async (article_id, data) => {
    try {
      await fetch(
        `${process.env.REACT_APP_SERVER_URL}/articles/${article_id}`,
        {
          method: 'put',
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + accessToken
          },
          body: JSON.stringify(data)
        }
      );
      await getArticles();
      handleCloseModal();
      createMessage('success', 'Article updated.');
    } catch (err) {
      console.log(err);
      handleCloseModal();
    }
  };

  const removeArticle = async article_id => {
    try {
      await fetch(
        `${process.env.REACT_APP_SERVER_URL}/articles/${article_id}`,
        {
          method: 'delete',
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + accessToken
          }
        }
      );
      await getArticles();
      createMessage('success', 'Article removed.');
    } catch (err) {
      console.log(err);
      createMessage('danger', 'Something went wrong.');
    }
  };

  const handleLoginFormSubmit = async data => {
    try {
      const resp = await fetch(
        `${process.env.REACT_APP_SERVER_URL}/auth/login`,
        {
          method: 'post',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        }
      );

      if (resp.status === 200) {
        const json = await resp.json();
        setAccessToken(json.access_token);
        setRefreshToken(true);
        window.localStorage.setItem('refreshToken', json.refresh_token);
        createMessage('success', 'You have logged in successfully.');
      } else {
        throw new Error(resp.statusText);
      }
    } catch (err) {
      console.log(err);
      createMessage('danger', 'Incorrect email and/or password.');
    }
  };

  const isAuthenticated = () => {
    if (accessToken || refreshToken) {
      return true;
    }
    return false;
  };

  const logoutUser = () => {
    window.localStorage.removeItem('refreshToken');
    setAccessToken(null);
    setRefreshToken(false);
    createMessage('success', 'You have logged out.');
  };

  const createMessage = (type, text) => {
    setMessageType(type);
    setMessageText(text);
    setTimeout(() => {
      removeMessage();
    }, 3000);
  };

  const removeMessage = () => {
    setMessageType(null);
    setMessageText(null);
  };

  const handleOpenModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  return (
    <main>
      <div className="wrapper">
        <div className="container">
          <NavBar logoutUser={logoutUser} isAuthenticated={isAuthenticated} />
          {messageType && messageText && (
            <Message
              messageType={messageType}
              messageText={messageText}
              removeMessage={removeMessage}
            />
          )}
          <Switch>
            <Route
              exact
              path="/"
              render={() => (
                <>
                  <Modal isOpen={showModal} style={modalStyles}>
                    <button
                      className="x"
                      aria-label="close"
                      onClick={handleCloseModal}
                    >
                      x
                    </button>
                    <AddArticle addArticle={addArticle} />
                  </Modal>
                  <ArticlesList
                    articles={articles}
                    handleOpenModal={handleOpenModal}
                    removeArticle={removeArticle}
                    isAuthenticated={isAuthenticated}
                  />
                </>
              )}
            />
            <Route
              path="/articles/:id"
              render={() => (
                <>
                  <Modal isOpen={showModal} style={modalStyles}>
                    <button
                      className="x"
                      aria-label="close"
                      onClick={handleCloseModal}
                    >
                      x
                    </button>
                    <UpdateArticle
                      articleId={articleId}
                      article={article}
                      updateArticle={updateArticle}
                      history={history}
                    />
                  </Modal>
                  <ArticleDetail
                    article={article}
                    getArticle={getArticle}
                    handleOpenModal={handleOpenModal}
                    isAuthenticated={isAuthenticated}
                  />
                </>
              )}
            />
            <Route exact path="/about" component={About} />
            <Route
              exact
              path="/admin"
              render={() => (
                <LoginForm
                  handleLoginFormSubmit={handleLoginFormSubmit}
                  isAuthenticated={isAuthenticated}
                />
              )}
            />
            <Route exact path="*" component={NotFound} />
          </Switch>
        </div>
        <br />
        <br />
      </div>
      <Footer />
    </main>
  );
};

export default App;
