import React from 'react';
import {cleanup, wait} from '@testing-library/react';

import NavBar from '../NavBar';

afterEach(cleanup);

describe('when unauthenticated', () => {
  const props = {
    logoutUser: () => {
      return true;
    },
    isAuthenticated: jest.fn().mockImplementation(() => false)
  };

  it('renders the default props', async () => {
    const {getByText, findByTestId} = renderWithRouter(<NavBar {...props} />);
    await wait(() => {
      expect(props.isAuthenticated).toHaveBeenCalledTimes(1);
    });
    expect((await findByTestId('nav-title')).innerHTML).toBe('Marko Miric');
    expect((await findByTestId('nav-about')).innerHTML).toBe('About');
  });

  it('renders', () => {
    const {asFragment} = renderWithRouter(<NavBar {...props} />);
    expect(asFragment()).toMatchSnapshot();
  });
});

describe('when authenticated', () => {
  const props = {
    logoutUser: () => {
      return true;
    },
    isAuthenticated: jest.fn().mockImplementation(() => true)
  };

  it('renders the default props', async () => {
    const {getByText, findByTestId} = renderWithRouter(<NavBar {...props} />);
    await wait(() => {
      expect(props.isAuthenticated).toHaveBeenCalledTimes(1);
    });
    expect((await findByTestId('nav-title')).innerHTML).toBe('Marko Miric');
    expect((await findByTestId('nav-about')).innerHTML).toBe('About');
    expect((await findByTestId('nav-logout')).innerHTML).toBe('Log Out');
  });

  it('renders', () => {
    const {asFragment} = renderWithRouter(<NavBar {...props} />);
    expect(asFragment()).toMatchSnapshot();
  });
});
