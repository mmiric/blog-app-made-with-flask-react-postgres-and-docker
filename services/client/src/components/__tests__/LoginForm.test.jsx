import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import {cleanup} from '@testing-library/react';

import LoginForm from '../LoginForm';

afterEach(cleanup);

const props = {
  handleLoginFormSubmit: () => {
    return true;
  },
  isAuthenticated: () => {
    return false;
  }
};

it('renders with default props', () => {
  const {getByLabelText, getByText} = renderWithRouter(
    <BrowserRouter>
      <LoginForm {...props} />
    </BrowserRouter>
  );

  const emailInput = getByLabelText('E-mail*');
  expect(emailInput).toHaveAttribute('type', 'email');
  expect(emailInput).not.toHaveValue();

  const passwordInput = getByLabelText('Password*');
  expect(passwordInput).toHaveAttribute('type', 'password');
  expect(passwordInput).not.toHaveValue();

  const buttonInput = getByText('Submit');
  expect(buttonInput).toHaveAttribute('type', 'submit');
});

it('renders', () => {
  const {asFragment} = renderWithRouter(
    <BrowserRouter>
      <LoginForm {...props} />
    </BrowserRouter>
  );
  expect(asFragment()).toMatchSnapshot();
});
