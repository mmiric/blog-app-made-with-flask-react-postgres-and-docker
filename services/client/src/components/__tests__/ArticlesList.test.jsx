import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import {render, cleanup} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import ArticlesList from '../ArticlesList';

afterEach(cleanup);

const articles = [
  {
    id: 1,
    title: 'Some fancy title 1',
    description: 'Some fancy description 1',
    body: '<p>Some fancy body 1</p>',
    author_id: 1
  },
  {
    id: 2,
    title: 'Some fancy title 2',
    description: 'Some fancy description 2',
    body: '<p>Some fancy body 2</p>',
    author_id: 1
  }
];

it('renders a title', () => {
  const {getByText} = render(
    <BrowserRouter>
      <ArticlesList
        articles={articles}
        removeArticle={() => true}
        isAuthenticated={() => true}
      />
    </BrowserRouter>
  );
  expect(getByText('Some fancy title 1')).toHaveClass('article-title');
  expect(getByText('Some fancy title 2')).toHaveClass('article-title');
});

it('renders', () => {
  const {asFragment} = render(
    <BrowserRouter>
      <ArticlesList
        articles={articles}
        removeArticle={() => true}
        isAuthenticated={() => true}
      />
    </BrowserRouter>
  );
  expect(asFragment()).toMatchSnapshot();
});

it('renders when authenticated', () => {
  const {asFragment} = render(
    <BrowserRouter>
      <ArticlesList
        articles={articles}
        removeArticle={() => true}
        isAuthenticated={() => true}
      />
    </BrowserRouter>
  );
  expect(asFragment()).toMatchSnapshot();
});
