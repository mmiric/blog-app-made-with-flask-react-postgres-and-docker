import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import {render, cleanup} from '@testing-library/react';

import AddArticle from '../AddArticle';

afterEach(cleanup);

const props = {
  addArticle: () => {
    return true;
  }
};

it('renders with default props', () => {
  const {getByLabelText, getByText} = render(
    <BrowserRouter>
      <AddArticle {...props} />
    </BrowserRouter>
  );

  const titleInput = getByLabelText('Title*');
  expect(titleInput).toHaveAttribute('type', 'text');
  expect(titleInput).not.toHaveValue();

  const descriptionInput = getByLabelText('Description*');
  expect(descriptionInput).toHaveAttribute('type', 'text');
  expect(descriptionInput).not.toHaveValue();

  const bodyInput = getByLabelText('Body*');
  expect(bodyInput).not.toHaveValue();

  const buttonInput = getByText('Submit');
  expect(buttonInput).toHaveAttribute('type', 'submit');
});

it('renders', () => {
  const {asFragment} = render(
    <BrowserRouter>
      <AddArticle {...props} />
    </BrowserRouter>
  );
  expect(asFragment()).toMatchSnapshot();
});
