import React from 'react';
import {render, cleanup} from '@testing-library/react';

import Message from '../Message';

afterEach(cleanup);

describe('when "messageType" is "success"', () => {
  const props = {
    messageType: 'success',
    messageText: 'Some random message text!',
    removeMessage: () => true
  };

  it('renders the default props', async () => {
    const {getByText, getByTestId} = render(<Message {...props} />);
    expect(getByTestId('message').innerHTML).toContain(
      'Some random message text!'
    );
    expect(getByText('Some random message text!')).toHaveClass(
      'message message-success'
    );
  });

  it('renders', () => {
    const {asFragment} = render(<Message {...props} />);
    expect(asFragment()).toMatchSnapshot();
  });
});

describe('when "messageType" is "danger"', () => {
  const props = {
    messageType: 'danger',
    messageText: 'Some random message text!',
    removeMessage: () => true
  };

  it('renders the default props', () => {
    const {getByText, getByTestId} = render(<Message {...props} />);
    expect(getByTestId('message').innerHTML).toContain(
      'Some random message text!'
    );
    expect(getByText('Some random message text!')).toHaveClass(
      'message message-danger'
    );
  });

  it('renders', () => {
    const {asFragment} = render(<Message {...props} />);
    expect(asFragment()).toMatchSnapshot();
  });
});
