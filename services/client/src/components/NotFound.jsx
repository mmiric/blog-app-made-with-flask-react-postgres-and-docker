import React from 'react';

const containerStyle = {
  display: 'grid',
  height: '75vh',
  placeItems: 'center'
};

const NotFound = () => (
  <div style={containerStyle}>
    <h1>404 Not Found</h1>
  </div>
);

export default NotFound;
