import React from 'react';
import PropTypes from 'prop-types';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import {Redirect} from 'react-router-dom';

const formContainerStyle = {
  display: 'grid',
  height: '70vh',
  placeItems: 'center'
};

const formTitleStyle = {
  textAlign: 'center'
};

const formLabelStyle = {
  display: 'block',
  marginTop: '0.5rem',
  color: '#888'
};

const formInputStyle = {
  display: 'block',
  width: '100%',
  padding: '0.4rem',
  fontSize: '1.2rem',
  border: '2px solid #333',
  borderRadius: '0.3rem'
};

const inputErrorStyle = {
  color: 'red',
  marginBottom: '0.5rem'
};

const LoginForm = props => {
  const formik = useFormik({
    initialValues: {
      email: '',
      password: ''
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email('Enter a valid email.')
        .required('Email is a required field.'),
      password: Yup.string().required('Password is a required field.')
    }),
    onSubmit: (values, {resetForm, setSubmitting}) => {
      props.handleLoginFormSubmit(values);
      resetForm();
      setSubmitting(false);
    }
  });

  if (props.isAuthenticated()) {
    return <Redirect to="/" />;
  }

  return (
    <div style={formContainerStyle}>
      <form onSubmit={formik.handleSubmit}>
        <h1 style={formTitleStyle}>Sign In</h1>
        <label htmlFor="input-email" style={formLabelStyle}>
          E-mail*
        </label>
        <input
          name="email"
          type="email"
          id="input-email"
          style={formInputStyle}
          {...formik.getFieldProps('email')}
        />
        {formik.touched.email && formik.errors.email ? (
          <div style={inputErrorStyle}>{formik.errors.email}</div>
        ) : null}
        <label htmlFor="input-password" style={formLabelStyle}>
          Password*
        </label>
        <input
          name="password"
          type="password"
          id="input-password"
          style={formInputStyle}
          {...formik.getFieldProps('password')}
        />
        {formik.touched.password && formik.errors.password ? (
          <div style={inputErrorStyle}>{formik.errors.password}</div>
        ) : null}
        <button type="submit" className="btn btn-dark btn-block">
          Submit
        </button>
      </form>
    </div>
  );
};

LoginForm.propTypes = {
  handleLoginFormSubmit: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.func.isRequired
};

export default LoginForm;
