import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {useParams} from 'react-router-dom';

const ArticleDetail = props => {
  const {id} = useParams();

  useEffect(() => {
    (async () => {
      await props.getArticle(id);
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  return (
    <div className="container">
      <div className="content">
        <br />
        {props.isAuthenticated() && (
          <>
            <br />
            <button
              onClick={props.handleOpenModal}
              className="btn btn-dark btn-block"
            >
              Update Article
            </button>
            <br />
            <br />
          </>
        )}
        <br />
        <h1>{props.article.title}</h1>
        <hr />
        <br />
        <div dangerouslySetInnerHTML={{__html: props.article.body}} />
      </div>
    </div>
  );
};

ArticleDetail.propTypes = {
  getArticle: PropTypes.func.isRequired
};

export default ArticleDetail;
