import React from 'react';
import PropTypes from 'prop-types';

const Message = props => {
  return (
    <div className="container">
      <span
        className={`message message-${props.messageType}`}
        data-testid="message"
      >
        {props.messageText}
      </span>
    </div>
  );
};

Message.propTypes = {
  messageType: PropTypes.string.isRequired,
  messageText: PropTypes.string.isRequired
};

export default Message;
