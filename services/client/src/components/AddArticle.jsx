import React from 'react';
import PropTypes from 'prop-types';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import {Editor} from '@tinymce/tinymce-react';

const formTitleStyle = {
  textAlign: 'center'
};

const formLabelStyle = {
  display: 'block',
  marginTop: '0.5rem',
  color: '#333'
};

const inputErrorStyle = {
  color: 'red',
  marginBottom: '0.5rem'
};

const AddArticle = props => {
  const formik = useFormik({
    initialValues: {
      title: '',
      description: '',
      body: ''
    },
    validationSchema: Yup.object({
      title: Yup.string().required('Article must have a title.'),
      description: Yup.string().required('Article must have a description')
    }),
    onSubmit: (values, {resetForm, setSubmitting}) => {
      props.addArticle(values);
      resetForm();
      setSubmitting(false);
    }
  });

  const handleEditorChange = e => {
    formik.values.body = e;
  };

  return (
    <div className="container">
      <form onSubmit={formik.handleSubmit}>
        <h1 style={formTitleStyle}>Add Article</h1>
        <br />
        <label htmlFor="input-title" style={formLabelStyle}>
          Title*
        </label>
        <input
          name="title"
          type="text"
          id="input-title"
          {...formik.getFieldProps('title')}
        />
        {formik.touched.title && formik.errors.title ? (
          <div style={inputErrorStyle}>{formik.errors.title}</div>
        ) : null}
        <label htmlFor="input-description" style={formLabelStyle}>
          Description*
        </label>
        <input
          name="description"
          type="text"
          id="input-description"
          {...formik.getFieldProps('description')}
        />
        {formik.touched.text && formik.errors.text ? (
          <div style={inputErrorStyle}>{formik.errors.text}</div>
        ) : null}
        <label htmlFor="input-body" style={formLabelStyle}>
          Body*
        </label>
        <Editor
          id="input-body"
          apiKey="4eywfq66c0xiha096esf4hiyifsipeld8g1w7sijcmnxs8tt"
          value={formik.values.body}
          init={{
            height: '50vh',
            plugins: [
              'advlist autolink lists link image charmap print preview anchor',
              'searchreplace visualblocks code fullscreen',
              'insertdatetime media table paste code help wordcount'
            ],
            toolbar:
              'file | undo redo | formatselect | ' +
              'bold italic backcolor | alignleft aligncenter ' +
              'alignright alignjustify | bullist numlist outdent indent | ' +
              'removeformat',
            branding: false,
            statusbar: false
          }}
          onEditorChange={handleEditorChange}
        />
        <br />
        <button type="submit" className="btn btn-dark btn-block">
          Submit
        </button>
      </form>
    </div>
  );
};

AddArticle.propTypes = {
  addArticle: PropTypes.func.isRequired
};

export default AddArticle;
