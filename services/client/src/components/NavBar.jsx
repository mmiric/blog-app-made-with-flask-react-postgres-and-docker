import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

const titleStyle = {
  fontWeight: 'bold'
};

const logoutStyle = {
  marginLeft: 'auto',
  padding: '0 1rem',
  cursor: 'pointer'
};

const NavBar = props => {
  let menu = (
    <li>
      <Link to="/about" data-testid="nav-about">
        About
      </Link>
    </li>
  );
  if (props.isAuthenticated()) {
    menu = (
      <>
        <li>
          <Link to="/about" data-testid="nav-about">
            About
          </Link>
        </li>
        <li style={logoutStyle}>
          <span onClick={props.logoutUser} data-testid="nav-logout">
            Log Out
          </span>
        </li>
      </>
    );
  }
  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <ul>
        <li>
          <Link to="/" style={titleStyle} data-testid="nav-title">
            Marko Miric
          </Link>
        </li>
        {menu}
      </ul>
    </nav>
  );
};

NavBar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.func.isRequired
};

export default NavBar;
