import React from 'react';

const Footer = () => (
  <footer className="footer">
    <small>
      <p>
        &copy; 2020 <a href="http://markomiric.io">markomiric.dev</a>.
      </p>
      <p>Developed by Marko Miric.</p>
    </small>
  </footer>
);

export default Footer;
