import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

const formContainerStyle = {
  display: 'grid',
  height: '10vh',
  placeItems: 'center'
};

const formInputStyle = {
  display: 'block',
  width: '100%',
  padding: '0.4rem',
  fontSize: '1.2rem',
  border: 'none',
  borderRadius: '0.3rem'
};

//2px solid #2c3e50

const updatedAtTextStyle = {
  opacity: '0.6'
};

const ArticlesList = props => {
  const [str, setStr] = useState('');

  const handleChange = e => {
    setStr(e.target.value);
  };

  const articles = !str
    ? props.articles
    : props.articles.filter(article =>
        article.title.toLowerCase().includes(str.toLocaleLowerCase())
      );

  return (
    <div className="container">
      <br />
      <div style={formContainerStyle}>
        <form>
          <input
            type="text"
            name="str"
            style={formInputStyle}
            placeholder="Search articles..."
            value={str}
            onChange={handleChange}
          />
        </form>
      </div>
      <br />
      {props.isAuthenticated() && (
        <>
          <br />
          <button
            onClick={props.handleOpenModal}
            className="btn btn-dark btn-block"
          >
            Add Article
          </button>
          <br />
          <br />
        </>
      )}
      {articles.map(article => {
        return (
          <div className="article" key={article.id}>
            <h2 className="article-title">{article.title}</h2>
            <p style={updatedAtTextStyle}>
              Last updated on {article.updated_date}
            </p>
            <p className="article-description">{article.description}</p>
            <Link
              to={`/articles/${article.id}`}
              className="btn btn-link btn-sm"
            >
              Read more
            </Link>
            {props.isAuthenticated() && (
              <button
                className="btn btn-danger btn-sm"
                onClick={async () => await props.removeArticle(article.id)}
              >
                Delete Article
              </button>
            )}
          </div>
        );
      })}
    </div>
  );
};

ArticlesList.propTypes = {
  articles: PropTypes.array.isRequired,
  removeArticle: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.func.isRequired
};

export default ArticlesList;
