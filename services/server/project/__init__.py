import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_bcrypt import Bcrypt

from project.exceptions import InvalidUsage


# instantiate the extensions
db = SQLAlchemy()
cors = CORS()
bcrypt = Bcrypt()


def create_app(script_info=None):

    # instantiate the app
    app = Flask(__name__)

    # set config
    app_settings = os.getenv("APP_SETTINGS")
    app.config.from_object(app_settings)

    # set up extensions
    db.init_app(app)
    cors.init_app(
        app,
        resources={r"*": {"origins": "*"}},
        headers=["Content-Type"],
        expose_headers=["Access-Control-Allow-Origin"],
        supports_credentials=True,
    )
    bcrypt.init_app(app)

    # register blueprints
    from project.users.views import users_blueprint

    app.register_blueprint(users_blueprint)

    from project.auth.views import auth_blueprint

    app.register_blueprint(auth_blueprint)

    from project.articles.views import articles_blueprint

    app.register_blueprint(articles_blueprint)

    # set up error handler
    app.errorhandler(InvalidUsage)(errorhandler)

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    return app


def errorhandler(error):
    response = error.to_json()
    response.status_code = error.status_code
    return response
