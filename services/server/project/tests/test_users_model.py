from project.users.models import User


def test_passwords_are_random(test_app, test_db, add_user):
    user_one = add_user("test1", "test1@test.com", "supersecret")
    user_two = add_user("test2", "test2@test.com", "supersecret")
    assert user_one.password != user_two.password


def test_encode_token(test_app, test_db, add_user):
    user = add_user("test1", "test1@test.com", "supersecret")
    token = user.encode_token(user.id, "access")
    assert isinstance(token, bytes)


def test_decode_token(test_app, test_db, add_user):
    user = add_user("test1", "test1@test.com", "supersecret")
    token = user.encode_token(user.id, "access")
    assert isinstance(token, bytes)
    assert User.decode_token(token) == user.id
