import json

import pytest

from project import bcrypt
from project.users.models import User


def test_all_users(test_app, test_db, add_user, get_access_token):
    add_user("test1", "test1@email.com", "supersecret")
    add_user("test2", "test2@email.com", "supersecret")
    add_user("registered_user", "registered_user@email.com", "supersecret")
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp = client.get("/api/users", headers={"Authorization": f"Bearer {token}"})
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 2
    assert "test1@email.com" in data["data"][0]["email"]
    assert "test2@email.com" in data["data"][1]["email"]
    assert "password" not in data["data"][0]
    assert "password" not in data["data"][1]


def test_add_user(test_app, test_db, add_user, get_access_token):
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp = client.post(
        "/api/users",
        data=json.dumps({"email": "test3@email.com", "password": "supersecret"}),
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 201
    assert "User added." in data["message"]


@pytest.mark.parametrize(
    "payload",
    [
        [{}],
        [{"email": "test@email.com", "password": "supersecret"}],
        [{"username": "test", "password": "supersecret"}],
        [{"email": "test@email.com", "username": "test"}],
    ],
)
def test_add_user_invalid_json(test_app, test_db, get_access_token, payload):
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp = client.post(
        "/api/users",
        data=json.dumps(payload),
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Invalid payload." in data["message"]


def test_add_user_duplicate_email(test_app, test_db, add_user, get_access_token):
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    client.post(
        "/api/users",
        data=json.dumps({"email": "test4@email.com", "password": "supersecret"}),
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    resp = client.post(
        "/api/users",
        data=json.dumps({"email": "test4@email.com", "password": "supersecret"}),
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Email already exists." in data["message"]


def test_single_user(test_app, test_db, add_user, get_access_token):
    user = add_user("test5", "test5@email.com", "supersecret")
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp = client.get(
        f"/api/users/{user.id}", headers={"Authorization": f"Bearer {token}"},
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert "test5@email.com" in data["data"]["email"]


def test_single_user_incorrect_id(test_app, test_db, add_user, get_access_token):
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp = client.get("/api/users/10000", headers={"Authorization": f"Bearer {token}"})
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "User not found." in data["message"]


def test_remove_user(test_app, test_db, add_user, get_access_token):
    test_db.session.query(User).delete()
    user = add_user("test1", "test1@email.com", "supersecret")
    add_user("test2", "test2@email.com", "supersecret")
    add_user("registered_user", "registered_user@email.com", "supersecret")
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp_one = client.get("/api/users", headers={"Authorization": f"Bearer {token}"})
    data = json.loads(resp_one.data.decode())
    assert resp_one.status_code == 200
    assert len(data["data"]) == 3
    resp_two = client.delete(
        f"/api/users/{user.id}", headers={"Authorization": f"Bearer {token}"}
    )
    data = json.loads(resp_two.data.decode())
    assert resp_two.status_code == 200
    assert "User deleted." in data["message"]
    resp_three = client.get("/api/users", headers={"Authorization": f"Bearer {token}"})
    data = json.loads(resp_three.data.decode())
    assert resp_three.status_code == 200
    assert len(data["data"]) == 2


def test_remove_user_incorrect_id(test_app, test_db, get_access_token):
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp = client.delete(
        "/api/users/10000", headers={"Authorization": f"Bearer {token}"}
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "User not found." in data["message"]


def test_update_user(test_app, test_db, add_user, get_access_token):
    user = add_user("test3", "test3@email.com", "supersecret")
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp_one = client.put(
        f"/api/users/{user.id}",
        data=json.dumps({"email": "new_email@email.com"}),
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    data = json.loads(resp_one.data.decode())
    assert resp_one.status_code == 200
    assert "User updated." in data["message"]
    resp_two = client.get(
        f"/api/users/{user.id}", headers={"Authorization": f"Bearer {token}"}
    )
    data = json.loads(resp_two.data.decode())
    assert resp_two.status_code == 200
    assert "new_email@email.com" in data["data"]["email"]


def test_update_user_with_passord(test_app, test_db, add_user, get_access_token):
    password_one = "notsupersecret"
    password_two = "supersecret"

    user = add_user("test10", "test10@email.com", password_one)
    assert bcrypt.check_password_hash(user.password, password_one)

    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp = client.put(
        f"/api/users/{user.id}",
        data=json.dumps({"email": "test20@email.com", "password": password_two}),
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert "User updated." in data["message"]


@pytest.mark.parametrize(
    "user_id, payload, status_code, message",
    [
        [1, {}, 400, "Invalid payload."],
        [
            10000,
            {"username": "test55", "email": "test55@email.com"},
            404,
            "User not found.",
        ],
    ],
)
def test_update_user_invalid_json(
    test_app, test_db, user_id, payload, status_code, message, get_access_token
):
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp = client.put(
        f"/api/users/{user_id}",
        headers={"Authorization": f"Bearer {token}"},
        data=json.dumps(payload),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == status_code
    assert message in data["message"]
