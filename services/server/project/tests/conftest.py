import pytest
import json

from project import create_app, db
from project.users.models import User
from project.articles.models import Article


@pytest.fixture(scope="module")
def test_app():
    app = create_app()
    app.config.from_object("project.config.TestingConfig")
    with app.app_context():
        yield app


@pytest.fixture(scope="module")
def test_db():
    db.create_all()
    yield db
    db.session.remove()
    db.drop_all()


@pytest.fixture(scope="module")
def add_user():
    def _add_user(username, email, password):
        user = User(username=username, email=email, password=password)
        db.session.add(user)
        db.session.commit()
        return user

    return _add_user


@pytest.fixture(scope="module")
def add_article():
    def _add_article(title, description, body, author_id):
        article = Article(
            title=title, description=description, body=body, author_id=author_id
        )
        db.session.add(article)
        db.session.commit()
        return article

    return _add_article


@pytest.fixture(scope="function")
def get_access_token():
    def _get_access_token(client, email, password):
        resp_login = client.post(
            "/api/auth/login",
            data=json.dumps({"email": email, "password": password}),
            content_type="application/json",
        )
        print(resp_login)
        return json.loads(resp_login.data.decode())["access_token"]

    return _get_access_token
