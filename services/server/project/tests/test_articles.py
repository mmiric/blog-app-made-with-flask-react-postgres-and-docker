import json

import pytest

from project.articles.models import Article


def test_all_articles(test_app, test_db, add_article, add_user):
    add_user("registered_user", "registered_user@email.com", "supersecret")
    add_article("test_title1", "test_description1", "<p>Test body1<p/>", 1)
    add_article("test_title2", "test_description2", "<p>Test body2<p/>", 1)
    client = test_app.test_client()
    resp = client.get("/api/articles")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 2
    assert "test_title1" in data["data"][0]["title"]
    assert "test_title2" in data["data"][1]["title"]
    assert "test_description1" in data["data"][0]["description"]
    assert "test_description2" in data["data"][1]["description"]
    assert "<p>Test body1<p/>" in data["data"][0]["body"]
    assert "<p>Test body2<p/>" in data["data"][1]["body"]
    assert data["data"][0]["author_id"] == 1
    assert data["data"][1]["author_id"] == 1


def test_add_article(test_app, test_db, add_article, get_access_token):
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp = client.post(
        "/api/articles",
        data=json.dumps(
            {
                "title": "test_title3",
                "description": "test_description3",
                "body": "<p>Test body3</p>",
                "author_id": 1,
            }
        ),
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 201
    assert "Article added." in data["message"]


@pytest.mark.parametrize(
    "payload",
    [
        [{}],
        [
            {
                "title": "test_title3",
                "description": "test_description3",
                "body": "<p>Test body3</p>",
            }
        ],
    ],
)
def test_add_article_invalid_json(test_app, test_db, get_access_token, payload):
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp = client.post(
        "/api/articles",
        data=json.dumps(payload),
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Invalid payload." in data["message"]


def test_add_article_duplicate_title(test_app, test_db, add_article, get_access_token):
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    client.post(
        "/api/articles",
        data=json.dumps(
            {
                "title": "test_title4",
                "description": "test_description4",
                "body": "<p>Test body4</p>",
                "author_id": 1,
            }
        ),
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    resp = client.post(
        "/api/articles",
        data=json.dumps(
            {
                "title": "test_title4",
                "description": "test_description4",
                "body": "<p>Test body4</p>",
                "author_id": 1,
            }
        ),
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Title already exists." in data["message"]


def test_single_article(test_app, test_db, add_article):
    article = add_article("test_title4", "test_description4", "<p>Test body4</p>", 1)
    client = test_app.test_client()
    resp = client.get(f"/api/articles/{article.id}")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert "test_title4" in data["data"]["title"]


def test_single_article_incorrect_id(test_app, test_db, add_article, get_access_token):
    client = test_app.test_client()
    resp = client.get("/api/articles/10000")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "Article not found." in data["message"]


def test_remove_article(test_app, test_db, add_user, add_article, get_access_token):
    test_db.session.query(Article).delete()
    article = add_article("test_title1", "test_description1", "<p>Test body1</p>", 1)
    add_article("test_title2", "test_description2", "<p>Test body2</p>", 1)
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp_one = client.get("/api/articles", headers={"Authorization": f"Bearer {token}"})
    data = json.loads(resp_one.data.decode())
    assert resp_one.status_code == 200
    assert len(data["data"]) == 2
    resp_two = client.delete(
        f"/api/articles/{article.id}", headers={"Authorization": f"Bearer {token}"}
    )
    data = json.loads(resp_two.data.decode())
    assert resp_two.status_code == 200
    assert "Article deleted." in data["message"]
    resp_three = client.get(
        "/api/articles", headers={"Authorization": f"Bearer {token}"}
    )
    data = json.loads(resp_three.data.decode())
    assert resp_three.status_code == 200
    assert len(data["data"]) == 1


def test_remove_article_incorrect_id(test_app, test_db, get_access_token):
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp = client.delete(
        "/api/articles/10000", headers={"Authorization": f"Bearer {token}"}
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "Article not found." in data["message"]


def test_update_article(test_app, test_db, add_article, get_access_token):
    article = add_article("test_title3", "test_description3", "<p>Test body3</p>", 1)
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp_one = client.put(
        f"/api/articles/{article.id}",
        data=json.dumps({"description": "new_test_description3"}),
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    data = json.loads(resp_one.data.decode())
    assert resp_one.status_code == 200
    assert "Article updated." in data["message"]
    resp_two = client.get(f"/api/articles/{article.id}")
    data = json.loads(resp_two.data.decode())
    assert resp_two.status_code == 200
    assert "new_test_description3" in data["data"]["description"]


@pytest.mark.parametrize(
    "article_id, payload, status_code, message",
    [
        [1, {}, 400, "Invalid payload."],
        [
            10000,
            {
                "title": "test_title55",
                "description": "test_description55",
                "body": "<p>Test body55</p>",
                "author_id": 1,
            },
            404,
            "Article not found.",
        ],
    ],
)
def test_update_article_invalid_json(
    test_app, test_db, article_id, payload, status_code, message, get_access_token
):
    client = test_app.test_client()
    token = get_access_token(client, "registered_user@email.com", "supersecret")
    resp = client.put(
        f"/api/articles/{article_id}",
        headers={"Authorization": f"Bearer {token}"},
        data=json.dumps(payload),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == status_code
    assert message in data["message"]
