import json

import pytest
from flask import current_app


def test_user_registration(test_app, test_db):
    client = test_app.test_client()
    resp = client.post(
        "/api/auth/register",
        data=json.dumps({"email": "test1@email.com", "password": "supersecret"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    print(data)
    assert resp.status_code == 201
    assert resp.content_type == "application/json"
    assert "Successfully registered." in data["message"]


def test_user_registration_duplicate_email(test_app, test_db, add_user):
    add_user("test2", "test2@email.com", "supersecret")
    client = test_app.test_client()
    resp = client.post(
        "/api/auth/register",
        data=json.dumps({"email": "test2@email.com", "password": "supersecret"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert resp.content_type == "application/json"
    assert "Email already exists." in data["message"]


@pytest.mark.parametrize(
    "payload",
    [
        [{}],
        [{"email": "test@email.com", "password": "supersecret"}],
        [{"username": "test", "password": "supersecret"}],
        [{"email": "test@email.com", "username": "test"}],
    ],
)
def test_user_registration_invalid_json(test_app, test_db, payload):
    client = test_app.test_client()
    resp = client.post(
        "/api/auth/register", data=json.dumps(payload), content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    print(data)
    assert resp.status_code == 400
    assert resp.content_type == "application/json"
    assert "Invalid payload." in data["message"]


def test_registered_user_login(test_app, test_db, add_user):
    add_user("test3", "test3@email.com", "supersecret")
    client = test_app.test_client()
    resp = client.post(
        "/api/auth/login",
        data=json.dumps({"email": "test3@email.com", "password": "supersecret"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert data["access_token"]
    assert data["refresh_token"]


def test_not_registered_user_login(test_app, test_db):
    client = test_app.test_client()
    resp = client.post(
        "/api/auth/login",
        data=json.dumps({"email": "test4@test.com", "password": "supersecret"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    print(data)
    assert resp.status_code == 404
    assert resp.content_type == "application/json"
    assert "User not found." in data["message"]


def test_valid_refresh(test_app, test_db, add_user):
    add_user("test5", "test5@email.com", "supersecret")
    client = test_app.test_client()
    resp_login = client.post(
        "/api/auth/login",
        data=json.dumps({"email": "test5@email.com", "password": "supersecret"}),
        content_type="application/json",
    )
    data = json.loads(resp_login.data.decode())
    refresh_token = json.loads(resp_login.data.decode())["refresh_token"]
    resp = client.post(
        "/api/auth/refresh",
        data=json.dumps({"refresh_token": refresh_token}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert data["access_token"]
    assert data["refresh_token"]


def test_invalid_refresh_expired_token(test_app, test_db, add_user):
    add_user("test6", "test6@email.com", "supersecret")
    current_app.config["REFRESH_TOKEN_EXPIRATION"] = -1
    client = test_app.test_client()
    resp_login = client.post(
        "/api/auth/login",
        data=json.dumps({"email": "test6@email.com", "password": "supersecret"}),
        content_type="application/json",
    )
    refresh_token = json.loads(resp_login.data.decode())["refresh_token"]
    resp = client.post(
        "/api/auth/refresh",
        data=json.dumps({"refresh_token": refresh_token}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert resp.content_type == "application/json"
    assert "Signature expired. Please log in again." in data["message"]


def test_invalid_refresh(test_app, test_db):
    client = test_app.test_client()
    resp = client.post(
        "/api/auth/refresh",
        data=json.dumps({"refresh_token": "Invalid"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert resp.content_type == "application/json"
    assert "Invalid token. Please log in again." in data["message"]


def test_user_status(test_app, test_db, add_user):
    add_user("test7", "test7@email.com", "supersecret")
    client = test_app.test_client()
    resp_login = client.post(
        "/api/auth/login",
        data=json.dumps({"email": "test7@email.com", "password": "supersecret"}),
        content_type="application/json",
    )
    token = json.loads(resp_login.data.decode())["access_token"]
    resp = client.get(
        "/api/auth/status",
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert "test7@email.com" in data["data"]["email"]
    assert "password" not in data["data"]


def test_invalid_status(test_app, test_db):
    client = test_app.test_client()
    resp = client.get(
        "/api/auth/status",
        headers={"Authorization": "Bearer invalid"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert resp.content_type == "application/json"
    assert "Invalid token. Please log in again." in data["message"]
