from marshmallow import Schema, fields, post_dump


class UserSchema(Schema):
    id = fields.Int(dump_only=True)
    username = fields.Str()
    email = fields.Email()
    password = fields.String(load_only=True)
    created_date = fields.DateTime(dump_only=True)
    updated_date = fields.DateTime()
    admin = fields.Boolean(dump_only=True)

    @post_dump
    def dump_data(self, data, **kwargs):
        if data["admin"] is not True:
            del data["admin"]
        return data

    class Meta:
        strict = True


user_schema = UserSchema()
user_schemas = UserSchema(many=True)
