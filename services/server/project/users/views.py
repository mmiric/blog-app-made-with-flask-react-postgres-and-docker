import datetime

from flask import Blueprint, request
from sqlalchemy import exc
from marshmallow import ValidationError

from project import db
from project.users.models import User
from project.users.serializers import user_schema, user_schemas
from project.auth.utils import authenticate
from project.exceptions import InvalidUsage

users_blueprint = Blueprint("users", __name__)


@users_blueprint.route("/api/users", methods=["GET"])
@authenticate
def get_users(current_user):
    users = User.query.all()
    data = user_schemas.dump(users)
    response_object = {"status": "success", "data": data}
    return response_object, 200


@users_blueprint.route("/api/users", methods=["POST"])
@authenticate
def add_user(current_user):
    post_data = request.get_json()
    if not post_data:
        raise InvalidUsage("Invalid payload.", 400)
    try:
        data = user_schema.load(post_data)
        email, password = data["email"], data["password"]
        user = User.query.filter_by(email=email).first()
        if not user:
            db.session.add(User(email=email, password=password))
            db.session.commit()
            response_object = {"status": "success", "message": "User added."}
            return response_object, 201
        else:
            raise InvalidUsage("Email already exists.", 400)
    except ValidationError:
        raise InvalidUsage("Invalid payload.", 400)
    except (exc.IntegrityError, ValueError):
        db.session.rollback()
        raise InvalidUsage("Invalid payload.", 400)


@users_blueprint.route("/api/users/<user_id>", methods=["GET"])
@authenticate
def get_user(current_user, user_id):
    user = User.query.filter_by(id=int(user_id)).first()
    if not user:
        raise InvalidUsage("User not found.", 404)
    else:
        data = user_schema.dump(user)
        response_object = {"status": "success", "data": data}
        return response_object, 200


@users_blueprint.route("/api/users/<user_id>", methods=["PUT"])
@authenticate
def update_user(current_user, user_id):
    post_data = request.get_json()
    print(post_data)
    if not post_data:
        raise InvalidUsage("Invalid payload.", 400)
    try:
        user = User.query.filter_by(id=int(user_id)).first()
        if not user:
            raise InvalidUsage("User not found.", 404)
        data = user_schema.load(post_data)
        if "email" in data:
            duplicate_email = User.query.filter_by(email=data["email"]).first()
            if duplicate_email:
                raise InvalidUsage("Email already exists.", 400)
            user.email = data["email"]
        if "username" in data:
            user.username = data["username"]
        if "password" in data:
            user.password = data["password"]
        user.updated_date = datetime.datetime.utcnow()
        db.session.commit()
        response_object = {"status": "success", "message": "User updated."}
        return response_object, 200
    except ValidationError:
        raise InvalidUsage("Invalid payload.", 400)
    except (exc.IntegrityError, ValueError):
        db.session.rollback()
        raise InvalidUsage("Invalid payload.", 400)


@users_blueprint.route("/api/users/<user_id>", methods=["DELETE"])
@authenticate
def delete_user(current_user, user_id):
    user = User.query.filter_by(id=int(user_id)).first()
    if not user:
        raise InvalidUsage("User not found.", 404)
    else:
        db.session.delete(user)
        db.session.commit()
        response_object = {"status": "success", "message": "User deleted."}
        return response_object, 200
