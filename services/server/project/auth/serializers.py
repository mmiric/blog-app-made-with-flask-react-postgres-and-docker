from marshmallow import Schema, fields


class UserAuthSchema(Schema):
    email = fields.Email(required=True, load_only=True)
    password = fields.String(required=True, load_only=True)

    class Meta:
        strict = True


class AuthTokenSchema(Schema):
    access_token = fields.Str(dump_only=True)
    refresh_token = fields.Str()

    class Meta:
        strict = True


user_auth_schema = UserAuthSchema()
auth_token_schema = AuthTokenSchema()
