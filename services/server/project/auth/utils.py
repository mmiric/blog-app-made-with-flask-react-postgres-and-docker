import jwt

from functools import wraps

from flask import request

from project.users.models import User
from project.users.serializers import user_schema
from project.exceptions import InvalidUsage


def authenticate(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        auth_header = request.headers.get("Authorization")
        if auth_header:
            try:
                access_token = auth_header.split(" ")[1]
                user_id = User.decode_token(access_token)
                user = User.query.filter_by(id=int(user_id)).first()
                if not user:
                    raise InvalidUsage("Invalid token.", 401)
                current_user = user_schema.dump(user)
                return f(current_user, *args, **kwargs)
            except jwt.ExpiredSignatureError:
                raise InvalidUsage("Signature expired. Please log in again.", 401)
            except jwt.InvalidTokenError:
                raise InvalidUsage("Invalid token. Please log in again.", 401)
        else:
            raise InvalidUsage("Token required.", 403)

    return decorated_function


def is_admin(user_id):
    user = User.query.filter_by(id=user_id).first()
    return user.admin
