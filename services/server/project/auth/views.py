import jwt

from flask import Blueprint, request
from sqlalchemy import exc
from marshmallow import ValidationError


from project import db, bcrypt
from project.users.models import User
from project.auth.serializers import user_auth_schema, auth_token_schema
from project.users.serializers import user_schema
from project.exceptions import InvalidUsage

auth_blueprint = Blueprint("auth", __name__)


@auth_blueprint.route("/api/auth/register", methods=["POST"])
def register_user():
    post_data = request.get_json()
    if not post_data:
        raise InvalidUsage("Invalid payload.", 400)
    try:
        data = user_auth_schema.load(post_data)
        email, password = data["email"], data["password"]
        user = User.query.filter_by(email=email).first()
        if user:
            raise InvalidUsage("Email already exists.", 400)
        new_user = User(email=email, password=password)
        db.session.add(new_user)
        db.session.commit()
        response_object = {
            "status": "success",
            "message": "Successfully registered.",
        }
        return response_object, 201
    except ValidationError:
        raise InvalidUsage("Invalid payload.", 400)
    except (exc.IntegrityError, ValueError):
        db.session.rollback()
        raise InvalidUsage("Invalid payload.", 400)


@auth_blueprint.route("/api/auth/login", methods=["POST"])
def login_user():
    post_data = request.get_json()
    if not post_data:
        raise InvalidUsage("Invalid payload.", 400)
    data = user_auth_schema.load(post_data)
    email, password = data["email"], data["password"]
    user = User.query.filter_by(email=email).first()
    if not user or not bcrypt.check_password_hash(user.password, password):
        raise InvalidUsage("User not found.", 404)
    access_token = user.encode_token(user.id, "access")
    refresh_token = user.encode_token(user.id, "refresh")
    tokens = {
        "access_token": access_token.decode(),
        "refresh_token": refresh_token.decode(),
    }
    response_object = auth_token_schema.dump(tokens)
    return response_object, 200


@auth_blueprint.route("/api/auth/refresh", methods=["POST"])
def validate_refresh_token():
    post_data = request.get_json()
    if not post_data:
        raise InvalidUsage("Invalid payload.", 400)
    data = auth_token_schema.load(post_data)
    refresh_token = data["refresh_token"]
    try:
        user_id = User.decode_token(refresh_token)
        user = User.query.filter_by(id=int(user_id)).first()
        if not user:
            raise InvalidUsage("Invalid token.", 401)
        access_token = user.encode_token(user.id, "access")
        refresh_token = user.encode_token(user.id, "refresh")
        tokens = {
            "access_token": access_token.decode(),
            "refresh_token": refresh_token.decode(),
        }
        response_object = auth_token_schema.dump(tokens)
        return response_object, 200
    except jwt.ExpiredSignatureError:
        raise InvalidUsage("Signature expired. Please log in again.", 401)
    except jwt.InvalidTokenError:
        raise InvalidUsage("Invalid token. Please log in again.", 401)


@auth_blueprint.route("/api/auth/status", methods=["GET"])
def get_user_status():
    auth_header = request.headers.get("Authorization")
    if auth_header:
        try:
            access_token = auth_header.split(" ")[1]
            user_id = User.decode_token(access_token)
            user = User.query.filter_by(id=int(user_id)).first()
            if not user:
                raise InvalidUsage("Invalid token.", 401)
            data = user_schema.dump(user)
            response_object = {"status": "success", "data": data}
            return response_object, 200
        except jwt.ExpiredSignatureError:
            raise InvalidUsage("Signature expired. Please log in again.", 401)
        except jwt.InvalidTokenError:
            raise InvalidUsage("Invalid token. Please log in again.", 401)
    else:
        raise InvalidUsage("Token required.", 403)
