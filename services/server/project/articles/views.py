import datetime

from flask import Blueprint, request
from sqlalchemy import exc
from marshmallow import ValidationError

from project import db
from project.articles.models import Article
from project.articles.serializers import article_schema, article_schemas
from project.auth.utils import authenticate
from project.exceptions import InvalidUsage

articles_blueprint = Blueprint("articles", __name__)


@articles_blueprint.route("/api/articles", methods=["GET"])
def get_articles():
    articles = Article.query.all()
    data = article_schemas.dump(articles)
    response_object = {"status": "success", "data": data}
    return response_object, 200


@articles_blueprint.route("/api/articles", methods=["POST"])
@authenticate
def add_article(current_user):
    post_data = request.get_json()
    if not post_data:
        raise InvalidUsage("Invalid payload.", 400)
    try:
        data = article_schema.load(post_data)
        title, description, body = data["title"], data["description"], data["body"]
        article = Article.query.filter_by(title=title).first()
        if article:
            raise InvalidUsage("Title already exists.", 400)
        db.session.add(
            Article(
                title=title,
                description=description,
                body=body,
                author_id=current_user["id"],
            )
        )
        db.session.commit()
        response_object = {"status": "success", "message": "Article added."}
        return response_object, 201
    except ValidationError:
        raise InvalidUsage("Invalid payload.", 400)
    except (exc.IntegrityError, ValueError):
        db.session.rollback()
        raise InvalidUsage("Invalid payload.", 400)


@articles_blueprint.route("/api/articles/<article_id>", methods=["GET"])
def get_article(article_id):
    article = Article.query.filter_by(id=int(article_id)).first()
    if not article:
        raise InvalidUsage("Article not found.", 404)
    else:
        data = article_schema.dump(article)
        response_object = {"status": "success", "data": data}
        return response_object, 200


@articles_blueprint.route("/api/articles/<article_id>", methods=["PUT"])
@authenticate
def update_article(current_user, article_id):
    post_data = request.get_json()
    if not post_data:
        raise InvalidUsage("Invalid payload.", 400)
    try:
        article = Article.query.filter_by(id=int(article_id)).first()
        if not article:
            raise InvalidUsage("Article not found.", 404)
        data = article_schema.load(post_data)
        if "title" in data:
            article.title = data["title"]
        if "description" in data:
            article.description = data["description"]
        if "body" in data:
            article.body = data["body"]
        article.updated_date = datetime.datetime.utcnow()
        db.session.commit()
        response_object = {"status": "success", "message": "Article updated."}
        return response_object, 200
    except ValidationError:
        raise InvalidUsage("Malformed request.", 422)
    except (exc.IntegrityError, ValueError):
        db.session.rollback()
        raise InvalidUsage("Invalid payload.", 400)


@articles_blueprint.route("/api/articles/<article_id>", methods=["DELETE"])
@authenticate
def delete_article(current_user, article_id):
    article = Article.query.filter_by(id=int(article_id)).first()
    if not article:
        raise InvalidUsage("Article not found.", 404)
    else:
        db.session.delete(article)
        db.session.commit()
        response_object = {"status": "success", "message": "Article deleted."}
        return response_object, 200
