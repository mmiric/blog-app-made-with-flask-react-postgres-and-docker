from marshmallow import Schema, fields


class ArticleSchema(Schema):
    id = fields.Int(dump_only=True)
    title = fields.Str()
    description = fields.Str()
    body = fields.Str()
    created_date = fields.DateTime(dump_only=True)
    updated_date = fields.DateTime()
    author_id = fields.Int()

    class Meta:
        strict = True


article_schema = ArticleSchema()
article_schemas = ArticleSchema(many=True)
