from sqlalchemy.sql import func
from sqlalchemy.orm import relationship

from project import db


class Article(db.Model):

    __tablename__ = "articles"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(128), nullable=False)
    description = db.Column(db.Text, nullable=False)
    body = db.Column(db.Text, nullable=False)
    created_date = db.Column(db.DateTime(timezone=True), default=func.now(), nullable=False)
    updated_date = db.Column(db.DateTime(timezone=True), onupdate=func.now())
    author_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)

    user = relationship("User", backref="articles")

    def __init__(self, title="", description="", body="", author_id=None):
        self.title = title
        self.description = description
        self.body = body
        self.author_id = author_id

    def __repr__(self):
        return f"<Article({self.title})>"
