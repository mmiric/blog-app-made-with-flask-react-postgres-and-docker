from flask.cli import FlaskGroup

from project import create_app, db
from project.users.models import User
from project.articles.models import Article

app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command("recreate_db")
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command("seed_db")
def seed_db():
    db.session.add(User(username="test1", email="test1@gmail.com", password="secret"))
    db.session.add(User(username="test2", email="test2@gmail.com", password="secret"))
    db.session.add(
        Article(
            title="Fake title 1",
            description="Fake description 1",
            body="Fake body 1",
            author_id=1,
        )
    )
    db.session.add(
        Article(
            title="Fake title 2",
            description="Fake description 2",
            body="Fake body 2",
            author_id=2,
        )
    )
    db.session.commit()


if __name__ == "__main__":
    cli()
